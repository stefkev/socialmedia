"use strict"
require("dotenv").config();
const neo4j = require("neo4j-driver").v1;

var host = "bolt://"+process.env.DB_HOST;
var usernaname = process.env.DB_USERNAME;
var password = process.env.DB_PASSWORD;

console.log(host);
console.log(usernaname);
console.log(password);

let driver = neo4j.driver(host, neo4j.auth.basic(usernaname, password));
let jsonFile = require("jsonfile");


let session = driver.session()


let file = __dirname + "/data.json";

jsonFile.readFile(file, (err, obj) => {
    if (err) {
        throw err;
    }



    process.nextTick(() => {
        obj.forEach((data) => {
            data.friends.forEach((friend) => {
                var query = "MATCH (a:Person { uid: " + data.id + " }), (b:Person { uid:" + friend + " }) CREATE (a)-[:KNOWS]->(b)"
                console.log(query)
                session.run(query).then((result) => {
                    session.close();
                })
            })
        })
        setTimeout(() => {
            process.exit(0)
        }, 5000)

    })

    obj.forEach((data) => {
        var query = "CREATE (n:Person { uid:" + data.id + ", full:\'"+data.firstName+" "+data.surname+"\', firstName:\'" + data.firstName + "\', surname:\'" + data.surname +
            "\', age:" + data.age + ", gender:\'" + data.gender + "\'})";
        console.log(query)
        session.run(query).then((result) => {
            session.close();
        });

    })
})

process.on('exit', (status) => {
    driver.close();
    if (status == 0) {
        console.log('successfully inserted records into DB')
    } else {
        console.log('error occurred, status: ' + status)
    }
})
