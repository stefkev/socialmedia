"use strict"


require('dotenv').config();
const Person = require("./lib/person.js")
const express = require('express');
const bodyParser = require("body-parser");
const pug = require("pug");
const async = require("async")

let port = process.env.PORT || 3000;

let app = express();

app.set("view engine", "pug");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.get('/', (req, res) => {
    res.render('index');
})

app.get('/search', (req, res) => {
    var full = req.query.name;
    //waterfall async
    //1) regular user search
    //2) friends of user
    //3) friend of friends
    //4)suggested friend
    //5) final render
    async.waterfall([
        (callback) => {
            var p = new Person()
            p.search(full).then((resolve, reject) => {
                callback(null, resolve)
            })
        },
        (person, callback) => {
            var p = new Person()
            p.friends(full).then((resolve, reject) => {
                callback(null, person, resolve)
            })
        },
        (person, friend, callback) => {
            var p = new Person()
            p.friendOfFriends(full).then((resolve, reject) => {
                callback(null, person, friend, resolve)
            })
        },
        (person, friend, friendOfFriends, callback) => {
            var p = new Person()
            p.suggestedFriends(full).then((resolve, reject) => {
                callback(null, person, friend, friendOfFriends, resolve)
            })
        }

    ], (err, person, friends, friendsOfFriend, suggestedFriends) => {
        //data to render to page
        var data = {}

        //regular data of user
        data = {
            full: person[0]._fields[0].properties.full,
            gender: person.gender = person[0]._fields[0].properties.gender,
            age: person[0]._fields[0].properties.age.low
        }

        //friends of user
        data.friends = [];

        friends.forEach((friend) => {
            data.friends.push(friend._fields[0])
        })
        //friends of friend
        data.fof = [];
        friendsOfFriend.forEach((fof) => {
            data.fof.push(fof._fields[0])
        })
        //sugested friend
        data.suggestedFriends = []

        suggestedFriends.forEach((fof) => {
            data.suggestedFriends.push(fof._fields[0])
        })

        console.log(data)

        res.render('search', data)
    })
})

app.listen(port, () => {
    console.log("Server started on port " + port)
})
