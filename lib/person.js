"use strict"

const db = require('./db.js')

class Person {
    //return regular user 
    search(fullName) {
        return new Promise((resolve, reject) => {
            console.log('calling search')
            var query = ("MATCH (p:Person) WHERE p.full =~ '(?i)" + fullName + "' RETURN p")
            db.search(query, (err, result) => {
                if (err) {
                    console.log('rejected')
                    return reject(err)
                }
                return resolve(result)
            })
        })
    }
    friends(fullName) {
        //return friends of the user
        return new Promise((resolve, reject) => {
            var query = ("MATCH (p:Person)-[:KNOWS]->(t:Person) WHERE t.full=~ '(?i)" + fullName + "' RETURN p.full")
            db.search(query, (err, result) => {
                if (err) {
                    return reject(err)
                }

                return resolve(result)
            })
        })
    }
    friendOfFriends(fullName) {
        //return friend of friends, second step from original user, but selected friend does not know for original friend
        return new Promise((resolve, reject) => {
            var query = ("MATCH (p:Person)-[:KNOWS*2]->(t:Person) WHERE p.full =~ '(?i)" + fullName + "' AND NOT (t)-[:KNOWS]->(p) RETURN t.full")
            db.search(query, (err, result) => {
                if (err) {
                    return reject(err)
                }
                return resolve(result)
            })
        })
    }
    suggestedFriends(fullName) {
        //sugested friend
        return new Promise((resolve, reject) => {
            var query = ("MATCH (p: Person)-[:KNOWS]->(t:Person) where t.full=~ '(?i)"+fullName+"'"+
            " MATCH (c: Person)-[n:KNOWS]->(p) WITH c as chosen, t as origin, count(n) as friends"+
            " WHERE friends>=2 AND  NOT (chosen)-[:KNOWS]->(origin)  return chosen.full")
            db.search(query, (err, result) => {
                if (err) {
                    return reject(err)
                }
                return resolve(result)
            })
        })
    }
}

module.exports = Person;
