"use strict"

let neo4j = require("neo4j-driver").v1;
let driver = neo4j.driver("bolt://"+process.env.DB_HOST, neo4j.auth.basic(process.env.DB_USERNAME, process.env.DB_PASSWORD));



module.exports.search = function(query, cb) {
	let session = driver.session()
    session.run(query).then((result) => {
        session.close();
        cb(null, result.records)
    })
}

process.on('exit', (status) => {
    driver.exit();
})
